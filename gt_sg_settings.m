% List of default parameters no longer included in sg_calib_constants.m by default.
% These parameters are overwritten by anything in sg_calib_constants.m or the results of
% toolbox regressions.

% Flight model parameters
hd_a=3.83600000E-03;
hd_b=1.00780000E-02;
hd_c=9.85000000E-06;
therm_expan=7.05000000E-05;
temp_ref=1.50000000E+01;
abs_compress=4.18000000E-06;
pitchbias=0.00000000E+00;

% Motor conversion values
pitch_min_cnts=0;
pitch_max_cnts=5000;
roll_min_cnts=0;
roll_max_cnts=5000;
vbd_min_cnts=0;
vbd_max_cnts=5000;
vbd_cnts_per_cc=-4.07671;
pump_rate_intercept=1.275;
pump_rate_slope=-0.00015;
pump_power_intercept=17.4033;
pump_power_slope=0.017824;

% Searbird pressure and temperature conductivity corrections
cpcor=-9.5700E-08;
ctcor=3.2500E-06;
