function [eng,scicon] = gt_sg_sub_parse_scicon(glidernum,direct,dives)

%% I. Preload basic data
%% I.a. Load preliminary TT8 data
eng = gt_sg_sub_parse_eng(glidernum,direct,dives);

% Look for requested dives
if isempty(dives)
    dives = [eng.dive];
end


%% I.b. Load scicon data
% Get list of all files
scFiles = dir([direct,filesep,'sc*',filesep,'*.eng']);
% Select only desired files
divesNames = arrayfun(@(x) [num2str(glidernum),sprintf('%04d',x)],dives,'Uni',0);
scFiles = scFiles(arrayfun(@(x) contains(x.name,divesNames),scFiles));
diveNumStrPos = strfind(scFiles(1).name,['sc',num2str(glidernum)]) + [5:8];

% Determine which sensors are present
sensors = unique(...
    arrayfun(@(x) x.name( find(ismember(x.name,'_'),1,'first')+1 : find(ismember(x.name,'.'),1,'first')-1),...
    scFiles,'Uni',0));

% Concatenate data from each sensor
scData(numel(scFiles)) = struct;

count = numel(dives);
h = waitbar(0,['Loading Scicon data']);

for fstep = 1:numel(scFiles)
    scData(fstep).dive = str2double(scFiles(fstep).name(diveNumStrPos));
    scData(fstep).data = getdata([scFiles(fstep).folder,filesep,scFiles(fstep).name]);
    waitbar(fstep/count,h)
end

scicon(numel(eng)) = struct;
for fstep = 1:numel(scData)
    vars = fieldnames(scData(fstep).data);
    dive = scData(fstep).dive;
    
    for vstep = 1:numel(vars)
        variable = vars{vstep};
        
        if ~isfield(scicon(dive),variable)
            scicon(dive).(variable) = struct();
            scDevices.(variable) = true;
        end
        
        if isstruct(scicon(dive).(variable))
            vars2 = fieldnames(scData(fstep).data.(variable));
            for v2step = 1:numel(vars2)
                variable2 = vars2{v2step};
                if ~isfield(scicon(dive).(variable),variable2)
                    scicon(dive).(variable).(variable2) = [];
                end
                scicon(dive).(variable).(variable2) = ...
                    [scicon(dive).(variable).(variable2),scData(fstep).data.(variable).(variable2)];
                if ~strcmp(variable,'depth')
                    eng(dive).(variable).(variable2) = []; %% Here??
                end
            end
        else
            scicon(dive).(variable) = [scicon(dive).(variable),scData(fstep).data.(variable)];
        end
    end
end

delete(h);
% Copy eng data streams to scicon for comparison
fields = fieldnames(eng);
engLen = numel([eng.elaps_t]);
for fstep = 1:numel(fields)
    field = fields{fstep};
    if isfield(scDevices,field)
        if scDevices.(field)
            continue;
        end
    end
    if numel([eng.(field)]) == engLen % Copy basic variables and piloting info.
        for dstep = dives
            scicon(dstep).eng.(field) = eng(dstep).(field);
        end
        serDevices.(field) = false;
    elseif isstruct(eng(dive(1)).(field)) % Copy serdev sensors
        for dstep = dives
            scicon(dstep).(field) = eng(dstep).(field);
        end
        serDevices.(field) = true;
    end
end

%% II. Establish GUI
% Create
close all;
gui.window = figure('Visible','off',...
    'MenuBar','none',...
    'Units','normalized',...
    'Position',[0.05 0.05 0.9 0.9]...
    );

gui.hist = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.65 0.55 0.30 0.40],...
    'XGrid','on','YGrid','on',...
    'Parent',gui.window);

%% III. MAIN.M
% % Populate histogram
% fields = fieldnames(scicon);
% for fstep = 1:numel(fields)
%     field = fields{fstep};
%     try
%         hold(gui.hist,'on');
%         hist(cell2mat(arrayfun(@(x) diff(x.(field).time),scicon(dives),'Uni',false)),100,'FaceColor',fstep);
%     end
% end
baseTime = datenum(1970,1,1,0,0,0);
fields = fieldnames(scDevices);
engfields = fieldnames(serDevices);

fields = {fields{:}, engfields{cellfun(@(x) serDevices.(x),engfields)}}; % Move serdevices to scicon processing
engfields = engfields(cellfun(@(x) ~serDevices.(x),engfields)); % Keep only base level eng data streams

engfields = setxor([engfields{:},{'depth','elaps_t'}],{'depth','elaps_t'}); % Remove special variables

h = waitbar(0,['Interpolating Scicon data']);
for dstep = dives
    waitbar(dstep/count,h)
    times = [];
    for fstep = 1:numel(fields)
        field = fields{fstep};
        if isfield(scicon(dstep).(field),'time')
            times{fstep} = prctile(scicon(dstep).(field).time,[0 100]); % seconds since 1970
        end
    end
    
    if isempty(times)
        gt_sg_sub_echo({['ERROR: No scicon data found for dive ',num2str(dstep),'.'],'Please check directory.'});
        dives = setxor(dives,dstep);
        eng(dstep).dive = NaN;
        continue;
    end
    
    days2seconds = 60*60*24;
    seconds2days = 1/days2seconds;
    diveStart = datenum(eng(dstep).start(3)+1900,eng(dstep).start(1),eng(dstep).start(2),eng(dstep).start(4),eng(dstep).start(5),eng(dstep).start(6));
    secondsSince1970 = (diveStart - baseTime)*days2seconds;
    
    %     times = cellfun(@(x) (x*seconds2days + baseTime... % This is now in Matlab datenum format
    %         - diveStart) ... % This is now days since the start of the dive
    %         * days2seconds, ... % This is now in elaps_t format
    %         times,'Uni',0);
    times = cellfun(@(x) x - secondsSince1970, times , 'Uni', 0);
    
    t_lim = prctile([times{:},eng(dstep).elaps_t],[0 100]);
    t_ax = floor(t_lim(1)) : ceil(t_lim(2)); % new high-res elaps_t
    
    for fstep = 1:numel(fields)
        field = fields{fstep};
        try
            variables = fieldnames(scicon(dstep).(field));
            variables = setxor([variables{:},{'time','depth'}],{'time','depth'});
            if isempty(variables)
                continue
            end
            for vstep = 1:numel(variables)
                variable = variables{vstep};
                eng(dstep).(field).(variable) = ...
                    interp1(...
                    scicon(dstep).(field).time - secondsSince1970, ...
                    scicon(dstep).(field).(variable), ...
                    t_ax);
            end
        end
        
    end
    
    
    for fstep = 1:numel(engfields)
        field = engfields{fstep};
        eng(dstep).(field) = ...
            interp1(...
            scicon(dstep).eng.elaps_t, ...
            scicon(dstep).eng.(field), ...
            t_ax);
    end
    % mag from eng here, or above?
    % individual eng fields here.
    
    %     if isempty(scicon(dstep).depth)
    %         eng(dstep).depth = ...
    %             interp1(...
    %             eng(dstep).elaps_t, ...
    %             eng(dstep).depth, ...
    %             t_ax);
    %     else
    %         eng(dstep).depth = ...
    %             interp1(...
    %             scicon(dstep).depth.time - secondsSince1970, ...
    %             scicon(dstep).depth.depth, ...
    %             t_ax);
    %     end
    eng(dstep).depth = ...
        interp1(...
        eng(dstep).elaps_t, ...
        eng(dstep).depth, ...
        t_ax);
    eng(dstep).elaps_t = t_ax;
    eng(dstep).elaps_t_0000 = eng(dstep).elaps_t ...
    + datenum(0,0,0,eng(dstep).start(4),eng(dstep).start(5),eng(dstep).start(6)) ...
    * days2seconds;
end
delete(h);


%% HACK: Fill gaps at the start in temperature
fields = fieldnames(eng);
for dstep = dives
    
    nsamp = numel(eng(dstep).depth);
    
    for istep = 1:numel(fields)
        field = fields{istep};
        
        if isstruct(eng(dstep).(field))
            fields2 = fieldnames(eng(dstep).(field));
            for fstep = 1:numel(fields2)
                field2 = fields2{fstep};
                if numel(eng(dstep).(field).(field2)) == nsamp
                    n = find(isfinite(eng(dstep).(field).(field2)),1,'first');
                    eng(dstep).(field).(field2)(1:n) = eng(dstep).(field).(field2)(n);
                    
                    n = find(isfinite(eng(dstep).(field).(field2)),1,'last');
                    eng(dstep).(field).(field2)(n:end) = eng(dstep).(field).(field2)(n);
                end
            end
            
        elseif numel(eng(dstep).(field)) == nsamp
            n = find(isfinite(eng(dstep).(field)),1,'first');
            eng(dstep).(field)(1:n) = eng(dstep).(field)(n);
            
            n = find(isfinite(eng(dstep).(field)),1,'last');
            eng(dstep).(field)(n:end) = eng(dstep).(field)(n);
        end
    end
end

% Show window
%set(gui.window,'Visible','on');

%% IV. Local Subfunctions
%     function out = gt_downsample(dive,sensor,var)
%         out = interp1(scicon(dive).(sensor).time, ...
%             scicon(dive).(sensor).(var), ...
%             eng.elaps_t);
% end

%% FINISHED LOCAL
end

%% V. Remote Subfunctions
function out = getdata(filename)

% Open file
fid = fopen(filename,'r');

% Return empty arrray if fail
data = [];

if fid ~= -1 % If file exists
    
    % Parse version, glider number and start time etc.
    linedata = fgetl(fid);
    line_num = 1;
    while ~any(strfind(linedata,'start:')) && ischar(linedata)
        data.(linedata(2:strfind(linedata,':')-1)) = strtrim(linedata(strfind(linedata,':')+1:end));
        linedata = fgetl(fid);
        line_num = line_num+1;
    end
    data.(linedata(2:strfind(linedata,':')-1)) = strtrim(linedata(strfind(linedata,':')+1:end));
    
    % Convert dive number from string to double
    data.dive = str2double(data.container(regexp(data.container,'\d')));
    
    % Convert start date from string to array
    data.start = str2double(data.start);
    
    % Parse data column header
    data.columns = strsplit(data.columns,' '); % Standard eng files separate by ',' but scicon do it by ' '.
    
    % Import data
    dataArray = textscan(fid, repmat('%f',1,numel(data.columns)),...
        'Delimiter', ' ', 'ReturnOnError', false,...
        'CommentStyle','%');
    
    out = struct;
    % Ok, ok, I know eval is sloppy, but it's pretty bloody quick this way,
    % and it copes really well with the multi-level substructures...
    % Transpose so multi-dive concatenatable with [].
    for istep = 1:numel(dataArray)
        eval(['out.',data.columns{istep},' = dataArray{istep}'';']);
    end
end

fclose(fid);
end
