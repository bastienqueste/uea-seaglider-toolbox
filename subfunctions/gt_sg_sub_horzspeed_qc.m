% flagging poor quality speeds before estimating dacs
%
% Gillian Damerell, 14/7/2020

function data=gt_sg_sub_horzspeed_qc(data,istep)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if data.log(1).version~=6612
    data.flag(istep).model_horz_spd=nan(size(data.flag(istep).salinity));
else


    % index apogee from the information in the guidance and control data
    gc_data=data.log(istep).GC;
    elaps_t=data.eng(istep).elaps_t;
    is_apogee=0;
    apogee_start=nan;
    apogee_end=nan;

    for kk=1:size(gc_data,1)

        % first time it pumps should be the first half of apogee
        if (gc_data(kk,14)>0 && is_apogee==0)
            apogee_start=find(elaps_t > gc_data(kk,1),1,'first');
            is_apogee=1;
        % second time it pumps should be the second half of apogee
        elseif (gc_data(kk,14)>0 && is_apogee==1)
            apogee_end=find(elaps_t < gc_data(kk,11),1,'last');
            is_apogee=2;
        end
    end

    % can get some dives where it went into recovery before reaching apogee, so then it stops recording
    % the gc data and apogee_start and apogee_end will be empty.  Or it might go into recovery part way
    % through apogee, so apogee_start will be fine but apogee_end will be empty.

    if isnan(apogee_end)
        apogee_end=numel(elaps_t);
    end
    if isnan(apogee_start)
        apogee_start=numel(elaps_t)-1;
    end
    index_apogee=apogee_start:apogee_end;


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % index the diving period - starts after passing d_flare and after reaching a speed threshold, ends
    % at the start of apogee
    speedthreshold=10;  % cm/s (only want to include times when the glider is really moving)
    index_moving=find(abs(data.flight(istep).glide_spd)>speedthreshold,1,'first'); 
    index_flare=find(data.hydrography(istep).depth>data.log(istep).D_FLARE,1,'first');
    dive_start=nanmax([index_flare index_moving]);
    dive_end=index_apogee(1)-1;
    index_dive=dive_start:dive_end;

    % index the climb - starts from the end of apogee, ends when reach d_surf
    climb_start=index_apogee(end)+1;
    climb_end=find(data.hydrography(istep).depth>data.log(istep).D_SURF,1,'last');
    index_climb=climb_start:climb_end;


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % bad speed flag - set whenever the salinity flag is set, and also when the pitch is nan
    data.flag(istep).model_horz_spd=data.flag(istep).salinity;
    data.flag(istep).model_horz_spd(isnan(data.eng(istep).pitchAng))=1;
    index_spdflag=find(data.flag(istep).model_horz_spd);

    % when the bad speed flag is set, set the horizontal speed to zero (this will include points before
    % the start of the diving period and after the end of the climbing period)
    data.flight(istep).model_horz_spd(index_spdflag)=0;

    % when the bad speed flag is set during the diving period, set the horizontal speed to the average
    % speed during the dive (average after removing points flagged as bad speed)
    ave_hspd_dive=nanmean(data.flight(istep).model_horz_spd(setdiff(index_dive,index_spdflag)));
    data.flight(istep).model_horz_spd(intersect(index_dive,index_spdflag))=ave_hspd_dive;

    % when the bad speed flag is set during the climb, set the horizontal speed to the average
    % speed during the climb (average after removing points flagged as a bad speed)
    ave_hspd_climb=nanmean(data.flight(istep).model_horz_spd(setdiff(index_climb,index_spdflag)));
    data.flight(istep).model_horz_spd(intersect(index_climb,index_spdflag))=ave_hspd_climb;

    % set horizontal speed to zero throughout apogee
    data.flight(istep).model_horz_spd(index_apogee)=0;


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % clear all these indices and markers - don't want them to linger from one dive to the next
    clear apogee_* dive_* climb_* index_* gc_data elaps_t ave_hspd*
    
end

end
