function dive_list = gt_sg_sub_check_dive_validity(data,dive_list)
% Called with one input, returns a list of dives capable of being processed
% (both eng and log file, more than 2 data points). In the future this will
% likely include GPS sanity checks.
%
% When called with an additional input array, it returns the subset of valid dives
% from that list.
%
% GT_SG BYQ 2016
if nargin == 1
    dive_list = 1:numel(data.log);
    echo = false;
else
    echo = true;
    dlist_in = dive_list;
end

% Has existing log and eng data
dive_list = intersect(dive_list,...
    intersect([data.log.dive],[data.eng.dive]));

% Has more than 2 data points
dive_list = dive_list(...
    arrayfun(@(x) numel(x.elaps_t)>2, data.eng(dive_list)));


if echo
    if dlist_in ~= dive_list
        gt_sg_sub_echo({'Rejecting dives from processing: ',num2str(setxor(dlist_in,dive_list))});
    end
end
end