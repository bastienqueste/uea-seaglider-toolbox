% flagging the whole dive dacs for issues such as using T_LOITER, missing CT data, etc.
%
% Gillian Damerell, 11/9/2020

function data=gt_sg_sub_dac_qc(data,istep)

% create a dac flag
data.flag(istep).dac=[0 0 0];


%%%%%%%%%%%%%%%%%%%%%%%%
% if the glider loitered at depth we are oversampling that depth, so the dacs will be suspect.

if data.log(istep).T_LOITER>0
    data.flag(istep).dac(1)=1;
end


%%%%%%%%%%%%%%%%%%%%%%
% if there's a lot of missing CT data the calculation of water density will be suspect, so the
% flight model regression will be suspect, so the dacs will be suspect.  This will include dives
% where the CT sail was switched off.

ratio_sal=numel(find(isnan(data.hydrography(istep).salinity)))/numel(data.hydrography(istep).salinity);
ratio_temp=numel(find(isnan(data.hydrography(istep).temp)))/numel(data.hydrography(istep).temp);

if (ratio_sal>0.3 || ratio_temp>0.3)
    data.flag(istep).dac(2)=1;
end


%%%%%%%%%%%%%%%%%%%%%%%
% large upwelling/downwelling speeds even after the flight model regression.  This is based on the
% flagging in MakeDiveProfiles.py on the basestation.

large_up_down_well_spd = 0.05; % [m/s] difference between observed and calculated w to suggest big heaving
ratio_largewelling = .1; % ratio of data points where there are big excursions (more than this is an issue)

diff_w=abs(data.flight(istep).glide_vert_spd-data.flight(istep).model_vert_spd);

if ((numel(find(diff_w>large_up_down_well_spd))/numel(diff_w)) > ratio_largewelling)
    data.flag(istep).dac(3)=1;
end

