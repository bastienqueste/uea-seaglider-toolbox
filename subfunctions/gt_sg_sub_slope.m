function data = gt_sg_sub_slope(data)
%
% data = gt_sg_sub_slope(data)
%
% Wrapper for the UW glide_slope.m routines which calculates a first
% approximation of glide slope and speed from pressure-derived vertical
% velocities, pitch and hydrodynamic parameters.
%
% All credits to original authors at the University of Washington
%
% B.Y.QUESTE Feb 2015
%

if numel(data.gt_sg_settings.hd_a) ~= numel(data.hydrography)
    hd_a = ones(size(data.hydrography)) .* data.gt_sg_settings.hd_a;
else
    hd_a = data.gt_sg_settings.hd_a;
end
if numel(data.gt_sg_settings.hd_b) ~= numel(data.hydrography)
    hd_b = ones(size(data.hydrography)) .* data.gt_sg_settings.hd_b;
else
    hd_b = data.gt_sg_settings.hd_b;
end
if numel(data.gt_sg_settings.hd_c) ~= numel(data.hydrography)
    hd_c = ones(size(data.hydrography)) .* data.gt_sg_settings.hd_c;
else
    hd_c = data.gt_sg_settings.hd_c;
end


if ~isfield(data.hydrography,'rho')
    gt_sg_sub_echo({'Calculating naive estimate of glider flight based on pressure change.',...
        'Output in flight substructure as "glide_spd" and "glide_slope".'});
    for istep = gt_sg_sub_check_dive_validity(data)
        data.flight(istep).glide_vert_spd = -gt_sg_sub_diff({data.hydrography(istep).depth,data.eng(istep).elaps_t}); % m.s-1
        
        [data.flight(istep).glide_slope, data.flight(istep).glide_spd, data.flight(istep).glide_horz_spd]... % degrees, m.s-1 ?, m.s-1 ?
            = glide_slope(...
            data.flight(istep).glide_vert_spd,... % m.s-1
            data.eng(istep).pitchAng,... % degrees
            hd_a(istep),...
            hd_b(istep),...
            hd_c(istep),...
            data.gt_sg_settings.rho0);
    end
else
    gt_sg_sub_echo({'Calculating improved estimate of glider flight based on pressure change.',...
        'Now using variable sigma0 for processed hydrograph data',...
        'Output in flight substructure as "glide_spd" and "glide_slope".'});
    for istep = gt_sg_sub_check_dive_validity(data)
        data.flight(istep).glide_vert_spd = -gt_sg_sub_diff({data.hydrography(istep).depth,data.eng(istep).elaps_t}); % m.s-1
        
        [data.flight(istep).glide_slope, data.flight(istep).glide_spd, data.flight(istep).glide_horz_spd]... % degrees, m.s-1 ?, m.s-1 ?
            = glide_slope(...
            data.flight(istep).glide_vert_spd,... % m.s-1
            data.eng(istep).pitchAng,... % degrees
            hd_a(istep),...
            hd_b(istep),...
            hd_c(istep),...
            data.hydrography(istep).rho);
    end
end

data.flight = data.flight(:);
end


% ********************************************************************************
% Calcculate the theoretical glide angle and speed baseed upon the input
% parameters.
% ********************************************************************************
function [theta, spdg, hspdg] = glide_slope(w_m_s_v, pitch, hd_a, hd_b, hd_c, rho0)
%     Compute the glide slope, base on observed vertical velocity (pressure change), vehicle pitch,
%     and hydrodynamic constants.  Assumes constant bouyancy throught the dive (rho0)
%
%     Input:
%         w - (observed) vertical velocity cm/s
%         pitch - observed vehicle pitch (radians, positive nose up)
%         calib_consts - that contain
%     			hd_a, hd_b, hd_c - hydrodynamic parameters for lift, drag, and induced drag
%                         rho - density of deep water (maximum density encountered)
%
%     Returns:
%         converged - whether the iterative solution converged
%         speed - total vehicle speed through the water (cm/s)
%         glide angle - radians, positive nose up
%         stalled_i_v - locations where stalled
%
% Updated as a Matlab translation of the relevant function in HydroModel.py
% from the Clownfish release of the Seaglider basestation software.

% physical constants
m2cm = 100;
cm2m = 0.01; % cm to m

w_cm_s_v = w_m_s_v * m2cm;
num_rows = numel(w_cm_s_v);
vehicle_pitch_rad_v = deg2rad(pitch);

% TODO: Double check hd_s and source it externally
hd_s = -1/4; % how the drag scales by shape
% No use of glider_length since not using buoyancy terms for lift/drag

% Compute initial total speed (u_initial_cm_s_v) based on observed pitch and vertical velocity
u_initial_cm_s_v = zeros(1, num_rows);
pitched_i_v = vehicle_pitch_rad_v ~= 0; % pitched_i_v = filter(lambda i: vehicle_pitch_rad_v[i] != 0.0, xrange(num_rows))
u_initial_cm_s_v(pitched_i_v) = w_cm_s_v(pitched_i_v)./sin(vehicle_pitch_rad_v(pitched_i_v));

% Compute constants used in hydrodynamic computations (for efficiency)
% These are used to compute the (inverted) performance factor (param) under the sqrt in Eqn 8
% 4/lambda*tan^2(theta), where lambda here incorporates a constant q
cx = 4.*hd_b.*hd_c;
cy = hd_a.*hd_a.*((rho0/2.0).^(-hd_s));
cz = cy./cx;
czr = cx./cy;

% Compute performance factor based on constant bouyancy
% We do this once to compute where the vehicle is flying (non-complex solns)
% NOTE: defn: q = (rho/2)*u^2 where u is estimated total velocity
% Thus, q^(-1/4) = (rho0/2)^(1/4)*(cm2m*u)^(2/4) = (rho0/2)^(1/4)*sqrt(cm2m*u)
perf_fac_v = tan(vehicle_pitch_rad_v) .* tan(vehicle_pitch_rad_v) .* sqrt( cm2m * abs(u_initial_cm_s_v) ) .*cz;

% Establish (initial) masks for stall versus non-stall values
% these are updated with additional stall points below
flying_v = zeros(1, num_rows); % assume all stalled
non_stalled_i_v = perf_fac_v > 1; % non_stalled_i_v = filter(lambda i: perf_fac_v[i] > 1, xrange(num_rows))
flying_v(non_stalled_i_v) = 1; % flying

% First, initialize counter and test delta
theta_rad_v = vehicle_pitch_rad_v;

% Iterate on glide angle until convergence or loop limit
count = 21; % bin_fit 'glideslope' goes from 1 to 20

for loop_counter = 1:count
    theta_prev_rad_v = theta_rad_v;  % Store the previous iteration
    %%%%%%%%%%%%%
    flying_v(w_cm_s_v .* sin(theta_rad_v) < 0) = 0; % stalled
    
    
    % compute non-inverted "param" for Eqn 8
    factor = czr.* ...
        (1./(tan(theta_rad_v) .* tan(theta_rad_v) .* ...
        sqrt(cm2m .* w_cm_s_v ./ sin(theta_rad_v))));
                    % flying; compute attack angle (alpha) using Eqn 8.  Eqn 7 ignored because of constant bouyancy assumption
                % minus sign critical here
    alpha = (-0.5 * hd_a * tan(theta_rad_v) .* (1.0 - sqrt(1.0 - factor))) ./ hd_c;
                % improve our guess about glide angle based on new attack angle
                % defn: pitch = glide_angle + attack-angle
    flying_and_lowFactor =  factor <= 1.0 & flying_v;
    theta_rad_v(flying_and_lowFactor) = vehicle_pitch_rad_v(flying_and_lowFactor) - deg2rad(alpha(flying_and_lowFactor));
    
    if max(theta_prev_rad_v(flying_and_lowFactor) - theta_rad_v(flying_and_lowFactor) < 0.0001)
       break; 
    end
    
%%%%%%%%%%%%%% SAME AS ABOVE BUT SLOW WITH FOR LOOPS %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% PRESERVED FOR TESTING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     for istep = 1:num_rows
%         flying_v(w_cm_s_v .* sin(theta_rad_v) < 0) = 0; % stalled
%         delta_theta = 0;
%         
%         if flying_v(istep)
%             % compute non-inverted "param" for Eqn 8
%             factor = czr.* ...
%                 (1.0./(tan(theta_rad_v(istep)) .* tan(theta_rad_v(istep)) .* ...
%                 sqrt(cm2m .* w_cm_s_v(istep) ./ sin(theta_rad_v(istep)))));
% %             disp(num2str(nanmean(abs(factor))))
%             if factor <= 1.0
%                 % flying; compute attack angle (alpha) using Eqn 8.  Eqn 7 ignored because of constant bouyancy assumption
%                 % minus sign critical here
%                 alpha = (-0.5 * hd_a * tan(theta_rad_v(istep)) .* (1.0 - sqrt(1.0 - factor))) ./ hd_c;
% %                 disp(num2str(nanmean(abs(alpha))))
%                 % improve our guess about glide angle based on new attack angle
%                 % defn: pitch = glide_angle + attack-angle
%                 theta_rad_v(istep) = vehicle_pitch_rad_v(istep) - deg2rad(alpha);
%                 delta_theta = theta_prev_rad_v(istep) - theta_rad_v(istep);
%             else
%                 delta_theta = 0;
%                 flying_v(istep) = 0; % stalled
%             end
%         else
%             delta_theta = 0;
%         end
%         
%         max_delta_theta = max(abs(delta_theta), max_delta_theta);
%         % end of i loop, now check for any improvement in the estimates of theta_rad_v
%     end
%     
%     % log_debug("GSM iteration %d max delta theta = %f" % (loop_counter,max_delta_theta))
%     if max_delta_theta < 0.0001 % [rad]
%         converged = true;
%         break
%     end
%%%%%%%%%%%%%%%%%%%%%%%%% END OF LOOPED OLD VERSION %%%%%%%%%%%%%%%%%%%%%%%

    % end of loop_counter loop
end

% NOTE this correction isn't in diveplot_func.m
% Where model has singularities (flying_v[i] = 0), set theta_rad_v = pitch angle for computation below
stalled_i_v = flying_v == 0;
theta_rad_v(stalled_i_v) = vehicle_pitch_rad_v(stalled_i_v);

total_speed_cm_s_v = zeros(1, num_rows); % assume stalled everywhere....
pitched_i_v = vehicle_pitch_rad_v ~= 0;
total_speed_cm_s_v(pitched_i_v) = abs(w_cm_s_v(pitched_i_v) .* sqrt(1 + (1 ./ (tan(theta_rad_v(pitched_i_v)).^2)))); % except here...

% Determine other stalls...
stalled_i_v = total_speed_cm_s_v <= 0 | abs(theta_rad_v) < deg2rad(5); % TODO: Add more parameters here
total_speed_cm_s_v(stalled_i_v) = 0; % mark as stall
theta_rad_v(stalled_i_v) = 0; % going nowhere (this nails the pitch angle assignment above)

theta = rad2deg(theta_rad_v);
spdg = total_speed_cm_s_v * cm2m;
hspdg = spdg.*cosd(theta);

end