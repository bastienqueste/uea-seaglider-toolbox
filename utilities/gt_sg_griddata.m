function [VI,XI,YI,vi,vi_ind] = gt_sg_griddata(x,y,v,xi,yi,fun,interp,showplot)
% function [VI,XI,YI,vi] = gt_sg_griddata(x,y,v,xi,yi,fun,interp,showplot)
%
% Rapid data gridding function with variable function (mean, median, var,
% etc.). Returns NaNs for empty grid cells in output but has the option to
% perform 1-D interpolation along a specified dimension (0, none; 1,
% vertical; 2, horizontal).
%
% Inputs:
% x, y: x and y coordinates of values v. Need to be monotonically
%       increasing.
% v: values of variable to be gridded.
% xi, yi: x and y coordinates of resulting vi gridded values. Values are
%       calculated for at xi and vi coordinates, not between coordinates.
% fun: default is @mean; function to use on overlapping data in a grid
%       square. Other common functions are @median, @sum and @var.
% interp: default is 0 for none. Dimension along which to perform a 1-D
%       interpolation of the data. 1 is for vertical; 2 for horizontal;
%       3 for vertical then horizontal.
% showplot: default is 0 for no. Displays pcolor plot at the end (1) or
%       not (0). Input of 'ij' produces a plot with an inverted y-axis.
%
% Outputs:
% VI: numel(xi) by numel(yi) gridded values of v.
% XI, YI: numel(xi) by numel(yi) gridded values of x and y respectively.
% vi: value corresponding to coordinates (x,y) queried in VI calculed by 
%      @fun; same size as v. 
%
% By Bastien Y. Queste
% www.byqueste.com
% 15/07/2016
% ORV Sindhu Sadhana, BoBBLE cruise
%
%% START FUNCTION
% Parse input variables
if nargin < 8
    showplot = 0;
end
if nargin < 7
    interp = 0;
end
if nargin < 6
    fun = @mean;
end

% Identify bin edges from the data
edges_x = [xi(:); 2*xi(end) - xi(end-1)] - (xi(end) - xi(end-1))/2;
edges_y = [yi(:); 2*yi(end) - yi(end-1)] - (yi(end) - yi(end-1))/2;

% Identify non-NaN values
ind_valid = find(~isnan(x(:)+y(:)+v(:)));

% Identify in which grid all data points will go into
[~, bin_x] = histc(x(ind_valid),...
    edges_x);
[~, bin_y] = histc(y(ind_valid),...
    edges_y);
bin_x = bin_x(:); bin_y = bin_y(:); % Ensure these are columns.

% Identify points outside of the grid
ind_inGrid = find(...
    bin_x >= 1 & bin_x <= numel(xi) & ...
    bin_y >= 1 & bin_y <= numel(yi) ...
    ); 

% Grid the data:
VI = accumarray(...
    [bin_y(ind_inGrid), bin_x(ind_inGrid)],.... % Subscript indices indicating which bin the data is @fun'd into
    v(ind_valid(ind_inGrid)),... % Data to be gridded that are not NaN and in the grid.
    [numel(yi) numel(xi)],... % Define size of the input matrix (in case there are missing columns or rowa at the end
    fun,... % Apply function, generally @mean, but @median or @var are common
    NaN); % Fill empty grid cells with NaNs rather than 0's 

% Perform interpolation if requested
if interp ~= 0
    [rows, cols] = size(VI);
    if rem(interp,2) == 1
        for istep = 1:cols
            ind_valid2 = ~isnan(VI(:,istep));
            if sum(ind_valid2) > 3
                VI(:,istep) = interp1(yi(ind_valid2),VI(ind_valid2,istep),yi);
            end
        end
    end
    if interp >= 2
        for istep = 1:rows
            ind_valid2 = ~isnan(VI(istep,:));
            if sum(ind_valid2) > 3
                VI(istep,:) = interp1(xi(ind_valid2),VI(istep,ind_valid2),xi);
            end
        end
    end
end

% Grid x and y data if requested
if nargout > 1
    [XI, YI] = meshgrid(xi,yi);
end
if nargout > 3
    vi = nan(size(v));
    vi(ind_valid(ind_inGrid)) = VI(sub2ind(size(VI),bin_y(ind_inGrid),bin_x(ind_inGrid)));
end
if nargout > 4
    vi_ind = nan(size(v));
    vi_ind(ind_valid(ind_inGrid)) = sub2ind(size(VI),bin_y(ind_inGrid),bin_x(ind_inGrid));
end

% Plot gridded data if requested
if showplot ~= 0
    figure();
    imagesc(xi,yi,VI); shading flat; colorbar;
    if strcmp(showplot,'ij')
        axis ij;
    end
end
end