function data = gt_sg_sensors_qsppar(data)
% -=-=-=-=-=-=-=-=-=-=-=-=-=-
% COMPUTE
%  QSP PAR DATA
% -=-=-=-=-=-=-=-=-=-=-=-=-=-
%
% calibs = sg_calib_constants struct
% eng    = eng struct
%

%% Check for calibration data
% To deal with inconsistency of PAR constants structure name
if isfield(data.gt_sg_settings,'PAR_CalData')
    data.gt_sg_settings.PARCalData  = data.gt_sg_settings.PAR_CalData;
    data.gt_sg_settings = rmfield(data.gt_sg_settings, 'PAR_CalData');
elseif isfield(data.gt_sg_settings,'PARCalData')
else
    gt_sg_sub_echo({'No PAR calibration data. Cannot continue with processing'});
    return;
end

% To deal with inconsistency of PAR variable names
if isfield(data.gt_sg_settings.PARCalData, 'sensorDark')
    data.gt_sg_settings.PARCalData.darkOffset = data.gt_sg_settings.PARCalData.sensorDark;
    data.gt_sg_settings.PARCalData = rmfield(data.gt_sg_settings.PARCalData, 'sensorDark');
end

if isfield(data.gt_sg_settings.PARCalData, 'scalingFactor')
    data.gt_sg_settings.PARCalData.scaleFactor = data.gt_sg_settings.PARCalData.scalingFactor;
    data.gt_sg_settings.PARCalData = rmfield(data.gt_sg_settings.PARCalData, 'scalingFactor');
end

%% Identify sensor(s) that need to be processed
sensorPosition = strcmp(data.gt_sg_settings.sensorList(:,3),'gt_sg_sensors_qsppar.m');

%% List dives to process, no need for log files or currents for processing,
%% so all eng files should work
dives = [data.eng.dive];


Fields = strsplit(gt_sg_sub_find_nearest_string('PARuV',data.gt_sg_settings.sensorList{sensorPosition,4}),'.');
sensorfieldname = Fields{1};
datafieldname = Fields{2};

gt_sg_sub_echo({['Calculating QSP PAR data.']});

for dstep = [dives]
    data.hydrography(dstep).PAR = (data.eng(dstep).(sensorfieldname).(datafieldname) - data.gt_sg_settings.PARCalData.darkOffset/1000) ./ data.gt_sg_settings.PARCalData.scaleFactor;
end

end