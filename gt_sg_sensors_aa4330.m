function data = gt_sg_sensors_aa4330(data)

%% Identify sensor(s) that need to be processed
sensorPosition = find(strcmp(data.gt_sg_settings.sensorList(:,3),'gt_sg_sensors_aa4330.m'));

%% Identify lag correction coefficients

if isfield(data.gt_sg_settings,'aa4330_tau') && isfield(data.gt_sg_settings,'aa4330_tau_degC')
    tau_DO = [data.gt_sg_settings.aa4330_tau data.gt_sg_settings.aa4330_tau_degC];
    gt_sg_sub_echo({['Using a optode lag correction of ',num2str(tau_DO(1)),' s ',num2str(tau_DO(2)),' s.K^-1 (ref 20degC) as per Johannes Hahn''s thesis (2013).']});
else
    gt_sg_sub_echo({'No optode lag correction coefficients defined in sg_calib_const.m','Not performing any lag correction on the oxygen data','Suggest using optode_tau = 14.8 seconds and optode_tau_degC = -0.4 s.K^-1 as per Johannes Hahn''s thesis (2013).'});
    tau_DO = [0 0];
end

%% Check for missing coefficients
% List of necessary constants
optodeCalibrationConstants = {...
    'aa4330_TPhaseCoef0',...
    'aa4330_TPhaseCoef1',...
    'aa4330_TPhaseCoef2',...
    'aa4330_TPhaseCoef3',...
    'aa4330_FoilCoefA0',...
    'aa4330_FoilCoefA1',...
    'aa4330_FoilCoefA2',...
    'aa4330_FoilCoefA3',...
    'aa4330_FoilCoefA4',...
    'aa4330_FoilCoefA5',...
    'aa4330_FoilCoefA6',...
    'aa4330_FoilCoefA7',...
    'aa4330_FoilCoefA8',...
    'aa4330_FoilCoefA9',...
    'aa4330_FoilCoefA10',...
    'aa4330_FoilCoefA11',...
    'aa4330_FoilCoefA12',...
    'aa4330_FoilCoefA13',...
    'aa4330_FoilCoefB0',...
    'aa4330_FoilCoefB1',...
    'aa4330_FoilCoefB2',...
    'aa4330_FoilCoefB3',...
    'aa4330_FoilCoefB4',...
    'aa4330_FoilCoefB5',...
    'aa4330_FoilCoefB6'};

%% check missing coefs for SVU
if isfield(data.gt_sg_settings, 'optode_SVUCoef_enabled')
    if data.gt_sg_settings.optode_SVUCoef_enabled == 1
        gt_sg_sub_echo('using SVU optode equation');
        % overwrite coefs if using SVU mode
        optodeCalibrationConstants = {...
        'optode_PhaseCoef0',...
        'optode_PhaseCoef1',...
        'optode_PhaseCoef2',...
        'optode_PhaseCoef3',...
        'optode_SVUFoilCoef0',...
        'optode_SVUFoilCoef1',...
        'optode_SVUFoilCoef2',...
        'optode_SVUFoilCoef3',...
        'optode_SVUFoilCoef4',...
        'optode_SVUFoilCoef5',...
        'optode_SVUFoilCoef6'};
    else
        gt_sg_sub_echo('optode SVU found in sg_calib_constants but not enabled');
    end
end

%% Verify necessary calibration coeffcients are present
if any(~isfield(data.gt_sg_settings,optodeCalibrationConstants))
    gt_sg_sub_echo({'Missing the following optode calibration constants in sg_calib_constants.m:',...
        optodeCalibrationConstants{~isfield(data.gt_sg_settings,optodeCalibrationConstants)}});
    return
end

%% Remove need for B7 to 13, if they're missing, make them 0 by default.
%% No optode (as of 2021) uses these parameters anyway - TH
if ~isfield(data.gt_sg_settings, 'aa4330_FoilCoefB7')
    data.gt_sg_settings.aa4330_FoilCoefB7  = 0.000000E+00;
    data.gt_sg_settings.aa4330_FoilCoefB8  = 0.000000E+00;
    data.gt_sg_settings.aa4330_FoilCoefB9  = 0.000000E+00;
    data.gt_sg_settings.aa4330_FoilCoefB10 = 0.000000E+00;
    data.gt_sg_settings.aa4330_FoilCoefB11 = 0.000000E+00;
    data.gt_sg_settings.aa4330_FoilCoefB12 = 0.000000E+00;
    data.gt_sg_settings.aa4330_FoilCoefB13 = 0.000000E+00;
end

%% For each sensor needing to be processed...
for sPos = [sensorPosition]
    % Identify fields
    Fields = strsplit(gt_sg_sub_find_nearest_string('TCPhase',data.gt_sg_settings.sensorList{sPos,4}),'.');
    optfieldname = Fields{1};
    tcphasefield = Fields{2};
    gt_sg_sub_echo({['Proceeding with optode oxygen calculations using TPhase from the ',optfieldname,'.',tcphasefield,' column.']});
    
    Fields = strsplit(gt_sg_sub_find_nearest_string('Temp',data.gt_sg_settings.sensorList{sPos,4}),'.');
    optfieldname = Fields{1};
    tempfield = Fields{2};
    
    % choose which equation to use
    if isfield(data.gt_sg_settings, 'optode_SVUCoef_enabled')
        if data.gt_sg_settings.optode_SVUCoef_enabled == 1
            for istep=gt_sg_sub_check_dive_validity(data)
                data.hydrography(istep).oxygen = gt_sg_sub_calc_SVU_oxygen;
            end
        end
    else
        for istep=gt_sg_sub_check_dive_validity(data)
            data.hydrography(istep).oxygen = gt_sg_sub_calc_oxygen;
        end
    end
end
%% lag correction
%% important note, the Hahn lag correction assumes you're using a "Standard" optode foil, don't apply this lag correction for a fast optode. - TH
    function tphase_new = gt_sg_sub_lag_correct_hahn
            % it safest to always use the optode temperature here to avoid geometric lag effects between the position of the optode relative to the CT sail
            tau = tau_DO(1) + tau_DO(2)*(data.eng(istep).(optfieldname).(tempfield) - 20); % Sensor lag as per Johannes Hahn.
            ts = (data.hydrography(istep).time - data.hydrography(istep).time(1)) * 24 * 60 * 60; % Time array in seconds
            ts_hr = ts(1):5:ts(end); % Making high-res 1Hz time array
            %ts_hr = ts;
            
            % Interpolate TPhase to 1 Hz, apply filtering/smoothing in case of spikes
            tphase_hr = interp1(ts,gt_sg_sub_filter(data.eng(istep).(optfieldname).(tcphasefield),2,0),ts_hr);
            
            % Interpolate tau to 1 Hz, apply filtering/smoothing in case of spikes
            tau_hr = interp1(ts,gt_sg_sub_filter(tau,2,0),ts_hr);

            % Pre-allocate for speed
            tphase_new = tphase_hr;
            
            % Apply step-wise lag correction
            for jstep = 2:numel(tphase_hr)
                tphase_new(jstep) = ...
                    (tphase_hr(jstep) - (tphase_hr(jstep-1) .* ...
                    (exp(-(ts_hr(jstep)-ts_hr(jstep-1)) ./ tau_hr(jstep-1))))) ./ ...
                    (1-sum(exp(-(ts_hr(jstep)-ts_hr(jstep-1))./tau_hr(jstep-1))));
            end
            
            % Downsample back to original time array
            tphase_new = interp1(ts_hr,tphase_new,ts);
    end

%% calculate oxygen using SVU equation
    function oxygen = gt_sg_sub_calc_SVU_oxygen
        % which temperature?
        temperature = data.hydrography(istep).temp; % for a seaglider best use CT sail -TH
        % temperature = data.eng(istep).(optfieldname).(tempfield)

        if tau_DO(1) > 0
            tphase = gt_sg_sub_lag_correct_hahn;            
        else
            tphase = data.eng(istep).(optfieldname).(tcphasefield);
        end

        % apply TCphase to Calphase correction, on newer optodes (serial > 1000) TCphase == Calphase - TH
        calPhase = data.gt_sg_settings.optode_PhaseCoef0 + data.gt_sg_settings.optode_PhaseCoef1 .* tphase + ...
            data.gt_sg_settings.optode_PhaseCoef2 .* tphase.^2 + ...
            data.gt_sg_settings.optode_PhaseCoef3 .* tphase.^3;
        
        Ksv = data.gt_sg_settings.optode_SVUFoilCoef0 + ...
              data.gt_sg_settings.optode_SVUFoilCoef1 .* temperature + ...
              data.gt_sg_settings.optode_SVUFoilCoef2 .* temperature .^ 2;

        P0 = data.gt_sg_settings.optode_SVUFoilCoef3 + ...
             data.gt_sg_settings.optode_SVUFoilCoef4 .* temperature;
        Pc = data.gt_sg_settings.optode_SVUFoilCoef5 + ...
             data.gt_sg_settings.optode_SVUFoilCoef6 .* calPhase;

        oxygen_raw = ((P0 ./ Pc)-1) ./ Ksv; % this is uncorrected umol/l

         % SVU optodes ~should~ all use use ConcCoef, and optodes newer than 1000 will adjust ConCoef rather than Phasecoef during two point calibration.
         % apply them here, note the effect is the same as adjusting oxygen_offset and slope. TH
        if isfield(data.gt_sg_settings,'optode_ConcCoef0') & isfield(data.gt_sg_settings,'optode_ConcCoef1')
            oxygen_raw = data.gt_sg_settings.optode_ConcCoef0 + data.gt_sg_settings.optode_ConcCoef1 .* oxygen_raw;
        else
            gt_sg_sub_echo({'optode_ConcCoef are missing and would be expected for SVU optode, check sg_calib_constants against foil calibration!'});
        end
        
        % salinity and pressure correction
        % Garcia and Gordon, BK refit (umol l-1)
              B0 = -6.24097E-03;
              B1 = -6.93498E-03;
              B2 = -6.90358E-03;
              B3 = -4.29155E-03;
              C0 = -3.11680E-07;
            % Calculate scaled temperature
         % Ts = log((298.15 - data.hydrography(istep).temp)./(273.15 + data.hydrography(istep).temp));
         Ts = log((298.15 - temperature)./(273.15 + temperature));
        
            % salinity correction
         oxygen = oxygen_raw .* exp(data.hydrography(istep).salinity .* (B0 + B1 * Ts + B2 * Ts.^2 + B3 .* Ts.^3) + C0 .* data.hydrography(istep).salinity.^2);
                
            % Pressure correction:
         oxygen = oxygen .* (1.0 + ((0.032 .* data.hydrography(istep).pressure) ./ 1000.0));
         
            % convert to umol kg-1 using potential density
         oxygen = oxygen ./ (data.hydrography(istep).sigma0 ./ 1000);

        % apply secondary calibration if present in sg_calib - eg if done Winkler titrations to
        % compare with
        % per Bittig2018 in the absence of calibration data spanning the full parameter space
        % it is better to apply a slope correction (gain) rather than just an offset.
        optode_offset = 0.0;
        optode_slope = 1.0;
        if isfield(data.gt_sg_settings,'optode_offset')
            optode_offset = data.gt_sg_settings.optode_offset;
        end
        if isfield(data.gt_sg_settings,'optode_slope')
            optode_slope = data.gt_sg_settings.optode_slope;
        end
        if isfield(data.gt_sg_settings,'optode_slope') | isfield(data.gt_sg_settings,'optode_offset')
            gt_sg_sub_echo({['applying optode calibration correction (',num2str(optode_offset),' + x',num2str(optode_slope),') from sg_calib_const.m']});
            oxygen = optode_offset + oxygen .* optode_slope;
        end
    
    end % gt_sg_sub_calc_SVU_oxygen.m

%% calculate oxygen using mk2 optode equation (4330 series)
    function oxygen = gt_sg_sub_calc_oxygen

        % which temperature?
        temperature = data.hydrography(istep).temp; % for a seaglider best use CT sail -TH
        % temperature = data.eng(istep).(optfieldname).(tempfield)
        
        if tau_DO(1) > 0
            tphase = gt_sg_sub_lag_correct_hahn;            
        else
            tphase = data.eng(istep).(optfieldname).(tcphasefield);
        end

        % apply TCphase to Calphase correction, on newer optodes (serial > 1000) TCphase == Calphase - TH
        calPhase = data.gt_sg_settings.aa4330_TPhaseCoef0 + data.gt_sg_settings.aa4330_TPhaseCoef1 .* tphase + ...
            data.gt_sg_settings.aa4330_TPhaseCoef2 .* tphase.^2 + ...
            data.gt_sg_settings.aa4330_TPhaseCoef3 .* tphase.^3;
        
        % Calculate DELTA-p
        dp = data.gt_sg_settings.aa4330_FoilCoefB1 +...
            temperature .* (data.gt_sg_settings.aa4330_FoilCoefB2 + ...
            temperature .* (data.gt_sg_settings.aa4330_FoilCoefB3 + ...
            temperature .* (data.gt_sg_settings.aa4330_FoilCoefB4 + ...
            temperature .* (data.gt_sg_settings.aa4330_FoilCoefB5 + ...
            temperature .* data.gt_sg_settings.aa4330_FoilCoefB6)))) + ...
            calPhase.* (...
            (data.gt_sg_settings.aa4330_FoilCoefA10 +...
            temperature .* (data.gt_sg_settings.aa4330_FoilCoefA11 +...
            temperature .* (data.gt_sg_settings.aa4330_FoilCoefA12 +...
            temperature .* (data.gt_sg_settings.aa4330_FoilCoefA13 +...
            temperature .* data.gt_sg_settings.aa4330_FoilCoefB0)))) +...
            calPhase.* (...
            (data.gt_sg_settings.aa4330_FoilCoefA6 +...
            temperature .* (data.gt_sg_settings.aa4330_FoilCoefA7 +...
            temperature .* (data.gt_sg_settings.aa4330_FoilCoefA8 +...
            temperature .* data.gt_sg_settings.aa4330_FoilCoefA9))) +...
            calPhase .* (...
            (data.gt_sg_settings.aa4330_FoilCoefA3 +...
            temperature .* (data.gt_sg_settings.aa4330_FoilCoefA4 +...
            temperature .* data.gt_sg_settings.aa4330_FoilCoefA5)) +...
            calPhase .* (...
            data.gt_sg_settings.aa4330_FoilCoefA2 +...
            temperature .* data.gt_sg_settings.aa4330_FoilCoefA0 + ...
            calPhase .* data.gt_sg_settings.aa4330_FoilCoefA1))));
        
        
        % Benson and Krause coefficients (1984) for umol.kg output (recommended for oceanographic work)
        A0 = 5.80871;
        A1 = 3.20291;
        A2 = 4.17887;
        A3 = 5.10006;
        A4 = -9.86643-2;
        A5 = 3.80369;
        B0 = -7.01577e-3;
        B1 = -7.70028e-3;
        B2 = -1.13864e-2;
        B3 = -9.51519e-3;
        C0 = -2.75915e-7;
        
        % Calcate vapour pressure
        pvapour = exp(52.57 - (6690.9./(temperature + 273.15))-(4.681.*log(temperature + 273.15)));
        
        % Calculate Air Saturation
        airsat = (dp .* 100)./((1013.25 - pvapour) .* 0.20946);
        
        % Calculate scaled temperature
        Ts = log((298.15 - temperature)./(273.15 + temperature));
        
        % Calculate oxygen solubility
        lnC = A0 + Ts.*(A1 + Ts.*(A2 + Ts.*(A3 + Ts.*(A4 + A5.*Ts)))) +...
            data.hydrography(istep).salinity .*(B0 + Ts.*(B1 + Ts.*(B2 + B3.*Ts))) + ...
            C0 .* data.hydrography(istep).salinity .* data.hydrography(istep).salinity;
        solub = exp(lnC);
        
        % Oxygen concentration calculated as:
        oxygen = (solub .* airsat) ./ 100;
        
        % Pressure correction:
        oxygen = oxygen .* (1.0 + ((0.032 .* data.hydrography(istep).pressure) ./ 1000.0));

        % apply secondary calibration if present in sg_calib
        % per Bittig2018 in the absence of calibration data spanning the full parameter space
        % it is better to apply a slope correction (gain) rather than just an offset.
        optode_offset = 0.0;
        optode_slope = 1.0;
        if isfield(data.gt_sg_settings,'aa4330_offset')
            optode_offset = data.gt_sg_settings.aa4330_offset;
        end
        if isfield(data.gt_sg_settings,'aa4330_slope')
            optode_slope = data.gt_sg_settings.aa4330_slope;
        end
        if isfield(data.gt_sg_settings,'aa4330_slope') | isfield(data.gt_sg_settings,'aa4330_offset')
            gt_sg_sub_echo({['applying optode calibration correction (',num2str(optode_offset),' + x',num2str(optode_slope),') from sg_calib_const.m']});
            oxygen = optode_offset + oxygen .* optode_slope;
        end
        
    end % gt_sg_sub_calc_oxygen.m

end % gt_sg_sensors_aa4330.m
