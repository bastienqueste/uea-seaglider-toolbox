UEA Seaglider toolbox Change log


15/Sept/2020
Two new subfunctions added - gt_sg_sub_dac_qc and gt_sg_sub_horzspeed_qc.  Both are called by gt_sg_sub_currents

gt_sg_sub_dac_qc aims to flag suspect dive average currents (dac) on much the same basis as is done in MakeDiveProfiles.py on the basestation.  See full description of the flags in the 'How to use the UEA Seaglider Toolbox' documentation.

gt_sg_sub_horzspeed_qc creates a flag for the flight.model_horz_spd variable, and is again described in the 'How to use the UEA Seaglider Toolbox' documentation.  This is mostly used as part of the calculation of dac.  model_horz_spd is now also set to zero throughout apogee.

If you have a dataset processed using a previous version of the toolbox, it is not necessary to rerun it through the whole toolbox to make these changes.  Just do data=gt_sg_sub_currents(data) and that will create the new flags.


03/Sept/2020
Altered the subfunction gt_sg_sub_currents to correct for magnetic declination.  Line 10 altered to read:
[vv_s, uu_s] = pol2cart( (data.eng(istep).head + data.log(istep).GPS1(8)) .* pi/180 , data.flight(istep).model_horz_spd); % m.s-1
instead of:
[vv_s, uu_s] = pol2cart( data.eng(istep).head .* pi/180 , data.flight(istep).model_horz_spd); % m.s-1

The variable data.log(istep).GPS1(8) is the magnetic declination.  This correction works on all the historical UEA glider datasets, but note that if, in the future, the structure of the GPS strings changes, it may be necessary to alter this.

If you have a dataset processed using a previous version of the toolbox, it is not necessary to rerun it through the whole toolbox to apply this correction.  Just do data=gt_sg_sub_currents(data) and the magnetic declination correction will be applied to the dive average currents.

October 2022.  Correct processing for oxygen optodes using ‘SVU’ style calibrations added.

January 2023.  Added a ‘quick fix’ which allows Rev E gliders to be processed through the flight model regression, gt_sg_Rev_E_fixes.m, to be run after gt_sg_load_merge_data.m and before gt_sg_process_data.m




