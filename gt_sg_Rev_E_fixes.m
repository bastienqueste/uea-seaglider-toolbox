% recreating the eng file fields GC_phase, rollCtl, pitchCtl and vbdCC for Rev E gliders from data
% in log files.  This is a step along the way to being able to process Rev E gliders in the UEA 
% Seaglider toolbox, and will allow people to carry on using any existing code which depends on
% these fields.  Note - there is an obvious weakness in this code in that it relies on the 
% ordering of the GC sentences remaining the same - start seconds in the first position, flags in
% the second position, and end seconds in the 14th position, etc.  If this processing seems to break
% after a firmware update, check whether these sentences are still ordered the same way.
%
% Gillian Damerell, 04/01/2023

function data=gt_sg_Rev_E_fixes(data)

% create a list of valid dives - both log and eng files, and more than 2 data points
dives = gt_sg_sub_check_dive_validity(data);

for jj=1:numel(dives)
    dive_num=dives(jj);
    % extract GC data from log file
    log_GC=data.log(dive_num).GC;
    GC_st_times=log_GC(:,1);
    GC_end_times=log_GC(:,14);
    %GC_flags=log_GC(:,2);
    GC_vbdCC=log_GC(:,3);
    GC_pitchCtl=log_GC(:,4);
    GC_rollCtl=log_GC(:,5);
    
    % extract time since start of dive for each line in eng file
    elapse_t=data.eng(dive_num).elaps_t;
    
    
    % create GCPHASE where the default state is passive, i.e., 6 in Rev B usage
    GC_phase=6.*ones(size(data.eng(dive_num).depth));
    % create dummy variables for vbdCC, rollCtl and pitchCtl
    vbdCC=zeros(size(data.eng(dive_num).depth));
    rollCtl=vbdCC;
    pitchCtl=vbdCC;
    
    % loop over GC phases
    for kk=1:numel(GC_st_times)
        % change GCPHASE to 3 (rolling) between the start and end times of active GC.  (Note - of
        % course, it isn't rolling in all GC intervals, but it is in most of them.  I couldn't find
        % any documentation for Rev B which states how it decides whether to use the pitch, vbd or
        % roll code in GC intervals which included more than one of those operations, so the useage
        % of these codes is poorly explained anyway, so I decided not to bother trying to code for
        % different active GC operations.)
        index=find(elapse_t>=GC_st_times(kk) & elapse_t<=GC_end_times(kk));
        GC_phase(index)=3;
        if kk==numel(GC_st_times)
            index2=find(elapse_t>=GC_st_times(kk) & elapse_t<=elapse_t(end));
        else
            index2=find(elapse_t>=GC_st_times(kk) & elapse_t<GC_st_times(kk+1));
        end
        vbdCC(index2)=GC_vbdCC(kk);
        rollCtl(index2)=GC_rollCtl(kk);
        pitchCtl(index2)=GC_pitchCtl(kk);
    end
    
    % alter GCPHASE to show when the glider is passively turning
    index3=find(rollCtl~=0 & GC_phase==6);
    GC_phase(index3)=4;
    
    data.eng(dive_num).rollCtl=rollCtl;
    data.eng(dive_num).pitchCtl=pitchCtl;
    data.eng(dive_num).vbdCC=vbdCC;
    data.eng(dive_num).GC_phase=GC_phase;
    
end